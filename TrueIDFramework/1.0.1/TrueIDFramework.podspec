#
# Be sure to run `pod lib lint TrueIDFramework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'TrueIDFramework'
s.version          = '1.0.1'
s.summary          = 'SDK True Login for other application.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
TODO: Add long description of the pod here.
The SDK supports both Swift and Objective-C language.
DESC

s.homepage         = 'https://bitbucket.org/truedmp/trueid-sdk-4.0-ios'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/scree\nshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'Apinun Wongintawang' => 'apinun_won@truecorp.co.th' }
s.source           = { :git => 'git@bitbucket.org:truedmp/trueid-sdk-4.0-ios.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '8.3'

s.source_files = ['TrueIDFramework/Classes/**/*','TrueIDFramework/Helper/**/*']

#s.resource_bundles = {
#'TrueIDFramework' => ['TrueIDFramework/Assets/*','TrueIDFramework/Classes/*.json']
#}
s.resource_bundles = {
'TrueIDFramework' => ['TrueIDFramework/Assets/*',
'TrueIDFramework/**/*.xib',
'TrueIDFramework/**/*.ttf',
'TrueIDFramework/**/*.storyboard',
'TrueIDFramework/**/*.strings',
'TrueIDFramework/*.lproj/*.strings']
}

#s.public_header_files = 'TrueIDFramework/Classes/**/*.h'
s.frameworks = 'UIKit' #'TrueIDFramework' #'MapKit'           #fixed for framework.h not found

s.dependency 'SwiftyRSA/ObjC'
s.dependency 'JWTDecode'

end
